<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthData extends Model
{
    use HasFactory;

    public const ACCOUNT_ID = 'account_id';
    public const BASE_DOMAIN = 'base_domain';
    public const ACCESS_TOKEN = 'access_token';
    public const REFRESH_TOKEN = 'refresh_token';
    public const EXPIRE = 'expire';

    public const TABLE_NAME = 'auth_data';
    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::ACCOUNT_ID;

    public function getAccountId(): int
    {
        return (int)$this->getAttributeFromArray(self::ACCOUNT_ID);
    }

    public function getAccessToken(): string
    {
        return $this->getAttributeFromArray(self::ACCESS_TOKEN);
    }

    public function getRefreshToken(): string
    {
        return $this->getAttributeFromArray(self::REFRESH_TOKEN);
    }

    public function getExpires(): Carbon
    {
        return Carbon::createFromTimeString($this->getAttributeFromArray(self::EXPIRE));
    }

    public function getBaseDomain(): string
    {
        return $this->getAttributeFromArray(self::BASE_DOMAIN);
    }

    public function setAccountId(int $accountId): void
    {
        $this->setAttribute(self::ACCOUNT_ID, $accountId);
    }

    public function setAccessToken(string $accessToken): void
    {
        $this->setAttribute(self::ACCESS_TOKEN, $accessToken);
    }

    public function setRefreshToken(string $refreshToken): void
    {
        $this->setAttribute(self::REFRESH_TOKEN, $refreshToken);
    }

    public function setExpires(Carbon $expires): void
    {
        $this->setAttribute(self::EXPIRE, $expires->toDateTimeString());
    }

    public function setBaseDomain(string $baseDomain): void
    {
        $this->setAttribute(self::BASE_DOMAIN, $baseDomain);
    }
}

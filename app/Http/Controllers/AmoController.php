<?php

namespace App\Http\Controllers;

use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\Leads\Pipelines\PipelinesCollection;
use AmoCRM\Collections\UsersCollection;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Filters\LeadsFilter;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\Leads\Pipelines\PipelineModel;
use AmoCRM\Models\Leads\Pipelines\Statuses\StatusModel;
use App\Helpers\AmoCRMHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AmoController extends Controller
{
    public function linkedLeadsToContact(Request $request, AmoCRMHelper $amoCRMHelper)
    {
        $accountId = $request->post('account_id');
        $contactsId = $request->post('contacts_id');
        $amoCRMApiClient = $amoCRMHelper->authorize($accountId);
        $filterContacts = new ContactsFilter();
        $filterContacts->setIds($contactsId);
        $contacts = $amoCRMApiClient->contacts()->get($filterContacts, [ContactModel::LEADS]);
        $leadsId = [];
        /** @var ContactModel $contact */
        foreach ($contacts as $contact) {
            $leadsCollection = $contact->getLeads();
            /** @var LeadModel $lead */
            foreach ($leadsCollection as $lead) {
                $leadsId[] = $lead->getId();
            }
        }
        $filterLeads = new LeadsFilter();
        $filterLeads->setIds($leadsId);
        $leads = $amoCRMApiClient->leads()->get($filterLeads, [LeadModel::LOSS_REASON, LeadModel::LOST_STATUS_ID]);
        $pipelinesCollection = $amoCRMApiClient->pipelines()->get();
        $usersCollection = $amoCRMApiClient->users()->get();
        return response()->json(
            [
                'payload' => ['leads' => $this->buildResponseLeads($leads, $pipelinesCollection, $usersCollection)]
            ]
        );
    }

    public function buildResponseLeads(LeadsCollection $leadsCollection, PipelinesCollection $pipelinesCollection, UsersCollection $usersCollection)
    {
        $leads = [];
        /** @var LeadModel $lead */
        foreach ($leadsCollection as $lead) {
            /** @var PipelineModel $pipeline */
            $pipeline = $pipelinesCollection->getBy('id', $lead->getPipelineId());
            /** @var StatusModel $status */
            $status = $pipeline->getStatuses()->getBy('id', $lead->getStatusId());
            $leads[] = [
                'id' => $lead->getId(),
                'name' => $lead->getName(),
                'budget' => $lead->getPrice(),
                'responsible' => $usersCollection->getBy('id', $lead->getResponsibleUserId())->getName(),
                'pipeline' => [
                    'id' => $pipeline->getId(),
                    'name' => $pipeline->getName(),
                    'status' => [
                        'id' => $status->getId(),
                        'name' => $status->getName(),
                        'color' => $status->getColor(),
                    ]
                ]

            ];
        }
        return $leads;
    }
}

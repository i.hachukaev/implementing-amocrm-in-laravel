<?php

namespace App\Http\Controllers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Models\AuthData;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class AmoAuthController extends Controller
{
    public function install(Request $request, AmoCRMApiClient $amoCRMApiClient)
    {
        try {
            $amoCRMApiClient->setAccountBaseDomain($request->get('referer'));
            $token = $amoCRMApiClient->getOAuthClient()->getAccessTokenByCode($request->get('code'));
            $amoCRMApiClient->setAccessToken($token);
            $currentAccount = $amoCRMApiClient->account()->getCurrent();
            if ($currentAccount === null) {
                Log::error('Received new token has expired. Request: ' . $request->json());
                return $this->errorResponse('Получен токен с истекшим сроком действия');
            }
            $account = AuthData::query()->where(AuthData::ACCOUNT_ID, $currentAccount->getId())
                ->first();
            if ($account === null) {
                $account = new AuthData();
            }
            $account->setAccountId($currentAccount->getId());
            $account->setBaseDomain($amoCRMApiClient->getAccountBaseDomain());
            $account->setRefreshToken($token->getRefreshToken());
            $account->setAccessToken($token->getToken());
            $account->setExpires(Carbon::createFromTimestamp($token->getExpires()));
            $account->save();
            return response()
                ->json([
                   'success' => true,
                    'payload' => $account
                ]);
        } catch (Throwable $e) {
            Log::error('Request: :request' . PHP_EOL . $e, ['request' => json_encode($request->all(), JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)]);
            return $this->errorResponse('Ошибка при обработке данных на сервере');
        }
    }


    /**
     * @param string $message
     * @return string|null
     */
    private function errorResponse(string $message)
    {
        return '<div style="text-align: center; margin-top: 40px;"><h1 style="color:red;">Виджет не установлен</h1><p>' . $message . '</p></div>';
    }
}

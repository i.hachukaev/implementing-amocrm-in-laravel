<?php

namespace App\Providers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Helpers\AmoCRMHelper;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AmoApiProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AmoCRMApiClient::class, function (Application $app) {
            return new AmoCRMApiClient(env('AMO_CLIENT_ID'), env('AMO_CLIENT_SECRET'), env('AMO_REDIRECT_URI'));
        });

        $this->app->bind(AmoCRMHelper::class, function (Application $app) {
            return new AmoCRMHelper($app->make(AmoCRMApiClient::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Helpers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Models\AuthData;
use Carbon\Carbon;
use Exception;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

class AmoCRMHelper
{
    /** @var AmoCRMApiClient */
    private $amoCRMApiClient;
    private $isAuthorized = false;

    public function __construct(AmoCRMApiClient $amoCRMApiClient)
    {
        $this->amoCRMApiClient = $amoCRMApiClient;
    }

    /**
     * @throws Exception
     */
    public function authorize(int $accountId)
    {
        /** @var AuthData $authData */
        $authData = AuthData::query()->where(AuthData::ACCOUNT_ID, $accountId)->first();
        if ($authData === null) {
            throw new Exception('NOT AUTHORIZED');
        }
        $this->amoCRMApiClient->setAccountBaseDomain($authData->getBaseDomain());
        $token = new AccessToken([
            'baseDomain' => $authData->getBaseDomain(),
            'refresh_token' => $authData->getRefreshToken(),
            'access_token' => $authData->getAccessToken(),
            'expires' => $authData->getExpires()->getTimestamp(),
        ]);
        $this->amoCRMApiClient->setAccessToken($token)->onAccessTokenRefresh(
            function (AccessTokenInterface $accessToken, string $baseDomain) use ($authData) {
                $authData->setBaseDomain($baseDomain);
                $authData->setRefreshToken($accessToken->getRefreshToken());
                $authData->setAccessToken($accessToken->getToken());
                $authData->setExpires(Carbon::createFromTimestamp($accessToken->getExpires()));
                $authData->save();
            }
        );
        $this->isAuthorized = true;
        return $this->amoCRMApiClient;
    }
}

<?php

use App\Models\AuthData;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AuthData::TABLE_NAME, function (Blueprint $table) {
            $table->unsignedBigInteger(AuthData::ACCOUNT_ID)->primary()->unique();
            $table->string(AuthData::BASE_DOMAIN);
            $table->longText(AuthData::ACCESS_TOKEN);
            $table->longText(AuthData::REFRESH_TOKEN);
            $table->dateTime(AuthData::EXPIRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AuthData::TABLE_NAME);
    }
}

define(["jquery"], function($){
    const CustomWidget = function () {
        const self = this,
            system = self.system(),
            host = 'https://test.sverka95.ru',
            apiURI = host + '/api';

        this.linkedLeadsToContact = function () {
            return new Promise(function (succeed, fail) {
                $.ajax ({
                    url: apiURI + '/leads/contact/',
                    type: "post",
                    data: {
                        // Account
                        account_id: AMOCRM.constant("account").id,

                        // Id contacts to lead
                        contacts_id: AMOCRM.constant("card_element").contacts,
                    },
                    dataType: 'json',
                    success: function (res) {
                        succeed(res);
                    },
                    error: function (jqXHR, status, errorMsq) {
                        fail(jqXHR.responseJSON);
                    }
                });
            });
        };

        // Генерация шаблона
        this.getTemplate = function (template, callback) {
            template = template || "";

            return self.render({
                href: "/templates/" + template + ".twig",
                base_path: self.base_path,
                load: callback,
            });
        };

        // Добавление css
        this.appendCss = function (file) {
            if ($("link[href=\"" + self.base_path + file + "?v=" + self.params.version + "\"]").length) {
                return false;
            }
            $("head").append("<link type=\"text/css\" rel=\"stylesheet\" href=\"" + self.base_path + file + "?v=" + self.params.version + "\">");

            return true;
        };

        this.callbacks = {
            init: function(){
                self.base_path = host + "/widget/launch";
                // Стили виджета
                self.appendCss("/css/style.css");
                return true;
            },
            bind_actions: function(){
                return true;
            },
            render: function(){
                if (system.area === "lcard") {
                    if (typeof (AMOCRM.data.current_card) != "undefined") {
                        if (AMOCRM.data.current_card.id === 0) {
                            return false;
                        }
                    }
                    self.linkedLeadsToContact().then(function (res) {
                        self.getTemplate('lcard', function (data) {
                            let params = {
                                self: self,
                                leads: res.payload.leads,
                            };
                            self.render_template({
                                caption: {
                                    class_name: 'leads-contact-widget-title'
                                },
                                body: '',
                                render: data.render(params)
                            })
                        })
                    })

                }
                return true;
            },
            dpSettings: function(){},
            destroy: function(){},
            contacts: {selected: function(){}},
            leads: {selected: function(){}},
            settings: function(settings){
                settings.find('input[name="change_me"]')
                    .val(Math.random())
                    .change()
                    .closest('.widget_settings_block__item_field')
                    .hide();
            },
            onSave: function(){
                return true;
            }
        };
        return this;
    };
    return CustomWidget;
});
